﻿namespace proyectoclubpu
{
    partial class frm_Hoteles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Hoteles));
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.btn_buscar = new System.Windows.Forms.Button();
            this.Btn_EliminarH = new System.Windows.Forms.Button();
            this.Btn_AgregarH = new System.Windows.Forms.Button();
            this.Btn_CancelarH = new System.Windows.Forms.Button();
            this.Bnt_GuardarH = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(145, 77);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(160, 20);
            this.textBox1.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Tai Le", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(36, 142);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(108, 16);
            this.label4.TabIndex = 7;
            this.label4.Text = "Número telefónico";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Tai Le", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(77, 111);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 16);
            this.label2.TabIndex = 3;
            this.label2.Text = "Dirección";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Tai Le", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(36, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "Nombre del Hotel";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(145, 140);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(160, 20);
            this.textBox2.TabIndex = 5;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(145, 105);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(160, 20);
            this.textBox3.TabIndex = 6;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(51, 17);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(274, 20);
            this.textBox4.TabIndex = 12;
            // 
            // btn_buscar
            // 
            this.btn_buscar.BackgroundImage = global::proyectoclubpu.Properties.Resources.search;
            this.btn_buscar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_buscar.Location = new System.Drawing.Point(331, 16);
            this.btn_buscar.Name = "btn_buscar";
            this.btn_buscar.Size = new System.Drawing.Size(28, 21);
            this.btn_buscar.TabIndex = 13;
            this.btn_buscar.UseVisualStyleBackColor = true;
            // 
            // Btn_EliminarH
            // 
            this.Btn_EliminarH.BackColor = System.Drawing.Color.Transparent;
            this.Btn_EliminarH.BackgroundImage = global::proyectoclubpu.Properties.Resources.iconfinder_trash_4341321_120557;
            this.Btn_EliminarH.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Btn_EliminarH.Location = new System.Drawing.Point(457, 11);
            this.Btn_EliminarH.Name = "Btn_EliminarH";
            this.Btn_EliminarH.Size = new System.Drawing.Size(37, 31);
            this.Btn_EliminarH.TabIndex = 11;
            this.Btn_EliminarH.UseVisualStyleBackColor = false;
            // 
            // Btn_AgregarH
            // 
            this.Btn_AgregarH.BackgroundImage = global::proyectoclubpu.Properties.Resources._1486485557_add_create_new_more_plus_81188;
            this.Btn_AgregarH.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Btn_AgregarH.Location = new System.Drawing.Point(414, 12);
            this.Btn_AgregarH.Name = "Btn_AgregarH";
            this.Btn_AgregarH.Size = new System.Drawing.Size(37, 31);
            this.Btn_AgregarH.TabIndex = 10;
            this.Btn_AgregarH.UseVisualStyleBackColor = true;
            // 
            // Btn_CancelarH
            // 
            this.Btn_CancelarH.BackgroundImage = global::proyectoclubpu.Properties.Resources.ic_cancel_128_28318;
            this.Btn_CancelarH.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Btn_CancelarH.Location = new System.Drawing.Point(543, 12);
            this.Btn_CancelarH.Name = "Btn_CancelarH";
            this.Btn_CancelarH.Size = new System.Drawing.Size(37, 31);
            this.Btn_CancelarH.TabIndex = 9;
            this.Btn_CancelarH.UseVisualStyleBackColor = true;
            // 
            // Bnt_GuardarH
            // 
            this.Bnt_GuardarH.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Bnt_GuardarH.BackgroundImage")));
            this.Bnt_GuardarH.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Bnt_GuardarH.Location = new System.Drawing.Point(500, 11);
            this.Bnt_GuardarH.Name = "Bnt_GuardarH";
            this.Bnt_GuardarH.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.Bnt_GuardarH.Size = new System.Drawing.Size(37, 31);
            this.Bnt_GuardarH.TabIndex = 8;
            this.Bnt_GuardarH.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ScrollBar;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(39, 192);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(566, 205);
            this.dataGridView1.TabIndex = 14;
            // 
            // frm_Hoteles
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::proyectoclubpu.Properties.Resources._2;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(663, 450);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.btn_buscar);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.Btn_EliminarH);
            this.Controls.Add(this.Btn_AgregarH);
            this.Controls.Add(this.Btn_CancelarH);
            this.Controls.Add(this.Bnt_GuardarH);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBox3);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox1);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frm_Hoteles";
            this.Text = "Hoteles";
            this.Load += new System.EventHandler(this.Hoteles_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Button Bnt_GuardarH;
        private System.Windows.Forms.Button Btn_CancelarH;
        private System.Windows.Forms.Button Btn_AgregarH;
        private System.Windows.Forms.Button Btn_EliminarH;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Button btn_buscar;
        private System.Windows.Forms.DataGridView dataGridView1;
    }
}