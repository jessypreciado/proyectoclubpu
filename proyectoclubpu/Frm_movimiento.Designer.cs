﻿namespace proyectoclubpu
{
    partial class Frm_movimiento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frm_movimiento));
            this.btn_agregarm = new System.Windows.Forms.Button();
            this.btn_eliminarm = new System.Windows.Forms.Button();
            this.btn_guardarm = new System.Windows.Forms.Button();
            this.btn_cancelarm = new System.Windows.Forms.Button();
            this.txt_buscarmov = new System.Windows.Forms.TextBox();
            this.btn_buscar = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.txt_nombrem = new System.Windows.Forms.TextBox();
            this.dtg_movimiento = new System.Windows.Forms.DataGridView();
            this.txt_lbl = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dtg_movimiento)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_agregarm
            // 
            this.btn_agregarm.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_agregarm.BackgroundImage")));
            this.btn_agregarm.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_agregarm.Location = new System.Drawing.Point(498, 12);
            this.btn_agregarm.Name = "btn_agregarm";
            this.btn_agregarm.Size = new System.Drawing.Size(37, 31);
            this.btn_agregarm.TabIndex = 0;
            this.btn_agregarm.UseVisualStyleBackColor = true;
            this.btn_agregarm.Click += new System.EventHandler(this.btn_agregarm_Click);
            // 
            // btn_eliminarm
            // 
            this.btn_eliminarm.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_eliminarm.BackgroundImage")));
            this.btn_eliminarm.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_eliminarm.Location = new System.Drawing.Point(541, 12);
            this.btn_eliminarm.Name = "btn_eliminarm";
            this.btn_eliminarm.Size = new System.Drawing.Size(37, 31);
            this.btn_eliminarm.TabIndex = 1;
            this.btn_eliminarm.UseVisualStyleBackColor = true;
            this.btn_eliminarm.Click += new System.EventHandler(this.btn_eliminarm_Click);
            // 
            // btn_guardarm
            // 
            this.btn_guardarm.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_guardarm.Location = new System.Drawing.Point(584, 12);
            this.btn_guardarm.Name = "btn_guardarm";
            this.btn_guardarm.Size = new System.Drawing.Size(37, 31);
            this.btn_guardarm.TabIndex = 2;
            this.btn_guardarm.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btn_guardarm.UseVisualStyleBackColor = true;
            this.btn_guardarm.Click += new System.EventHandler(this.btn_guardarm_Click);
            // 
            // btn_cancelarm
            // 
            this.btn_cancelarm.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_cancelarm.BackgroundImage")));
            this.btn_cancelarm.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_cancelarm.Location = new System.Drawing.Point(627, 12);
            this.btn_cancelarm.Name = "btn_cancelarm";
            this.btn_cancelarm.Size = new System.Drawing.Size(37, 31);
            this.btn_cancelarm.TabIndex = 3;
            this.btn_cancelarm.UseVisualStyleBackColor = true;
            // 
            // txt_buscarmov
            // 
            this.txt_buscarmov.Location = new System.Drawing.Point(12, 18);
            this.txt_buscarmov.Name = "txt_buscarmov";
            this.txt_buscarmov.Size = new System.Drawing.Size(400, 20);
            this.txt_buscarmov.TabIndex = 4;
            // 
            // btn_buscar
            // 
            this.btn_buscar.BackgroundImage = global::proyectoclubpu.Properties.Resources.search;
            this.btn_buscar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_buscar.Location = new System.Drawing.Point(418, 18);
            this.btn_buscar.Name = "btn_buscar";
            this.btn_buscar.Size = new System.Drawing.Size(28, 21);
            this.btn_buscar.TabIndex = 14;
            this.btn_buscar.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Tai Le", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(22, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(120, 16);
            this.label3.TabIndex = 15;
            this.label3.Text = "Nombre Movimiento";
            // 
            // txt_nombrem
            // 
            this.txt_nombrem.Location = new System.Drawing.Point(149, 90);
            this.txt_nombrem.Name = "txt_nombrem";
            this.txt_nombrem.Size = new System.Drawing.Size(297, 20);
            this.txt_nombrem.TabIndex = 16;
            this.txt_nombrem.TextChanged += new System.EventHandler(this.txt_nombrem_TextChanged);
            // 
            // dtg_movimiento
            // 
            this.dtg_movimiento.BackgroundColor = System.Drawing.SystemColors.ScrollBar;
            this.dtg_movimiento.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtg_movimiento.Location = new System.Drawing.Point(12, 154);
            this.dtg_movimiento.Name = "dtg_movimiento";
            this.dtg_movimiento.Size = new System.Drawing.Size(646, 228);
            this.dtg_movimiento.TabIndex = 17;
            this.dtg_movimiento.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dtg_movimiento_CellDoubleClick);
            // 
            // txt_lbl
            // 
            this.txt_lbl.AutoSize = true;
            this.txt_lbl.Location = new System.Drawing.Point(627, 25);
            this.txt_lbl.Name = "txt_lbl";
            this.txt_lbl.Size = new System.Drawing.Size(13, 13);
            this.txt_lbl.TabIndex = 18;
            this.txt_lbl.Text = "0";
            this.txt_lbl.Click += new System.EventHandler(this.txt_lbl_Click);
            // 
            // Frm_movimiento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(684, 450);
            this.Controls.Add(this.txt_lbl);
            this.Controls.Add(this.dtg_movimiento);
            this.Controls.Add(this.txt_nombrem);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.btn_buscar);
            this.Controls.Add(this.txt_buscarmov);
            this.Controls.Add(this.btn_cancelarm);
            this.Controls.Add(this.btn_guardarm);
            this.Controls.Add(this.btn_eliminarm);
            this.Controls.Add(this.btn_agregarm);
            this.DoubleBuffered = true;
            this.Name = "Frm_movimiento";
            this.Text = "Frm_movimiento";
            this.Load += new System.EventHandler(this.Frm_movimiento_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtg_movimiento)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_agregarm;
        private System.Windows.Forms.Button btn_eliminarm;
        private System.Windows.Forms.Button btn_guardarm;
        private System.Windows.Forms.Button btn_cancelarm;
        private System.Windows.Forms.TextBox txt_buscarmov;
        private System.Windows.Forms.Button btn_buscar;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txt_nombrem;
        private System.Windows.Forms.DataGridView dtg_movimiento;
        private System.Windows.Forms.Label txt_lbl;
    }
}