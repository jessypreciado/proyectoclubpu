﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EntidadClubA;
using LogicaClubA;

namespace proyectoclubpu
{
    public partial class Frm_movimiento : Form
    {
        private movimientoManejador _movimientoManejador;
        private movimiento _movimiento;
        public Frm_movimiento()
        {
            InitializeComponent();
            _movimientoManejador = new movimientoManejador();
            _movimiento = new movimiento();

        }

        private void Frm_movimiento_Load(object sender, EventArgs e)
        {

            Buscarmov("");
            ControlBotones(true, false, false, true);
            ControlCuadros(false);
            limpiarCuadros();
        }
        private void Buscarmov(string filtro)//solo para data
        {
            dtg_movimiento.DataSource =_movimientoManejador.GetMovimiento(filtro);//pasar la lista 
        }
        private void ControlBotones(bool Nuevo, bool guardar, bool Cancelar, bool eliminar)//-1
        {
            btn_agregarm.Enabled = Nuevo;
            btn_eliminarm.Enabled = eliminar;
           btn_guardarm.Enabled = guardar;
            btn_cancelarm.Enabled = Cancelar;
        }
        private void ControlCuadros(bool activar)//0
        {
            txt_nombrem.Enabled = activar;
           
        }
        private void Modificarmov()//funcion para  modificar  la tabla 
        {
            ControlCuadros(true);
            ControlBotones(false, true, true, false);

            txt_lbl.Text = dtg_movimiento.CurrentRow.Cells["id_movimiento"].Value.ToString();//mayuscula del Get
            txt_nombrem.Text = dtg_movimiento.CurrentRow.Cells["nombre"].Value.ToString();
            

        }

        private void limpiarCuadros()
        {
            txt_lbl.Text = "0";
            txt_nombrem.Text = "";
           
        }
        private void Eliminarmov()//necesitamos el id int para que elimine el dato
        {
            var id_movimiento = dtg_movimiento.CurrentRow.Cells["id_movimiento"].Value;//DEPENDE DEL DQATA
            _movimientoManejador.eliminar(Convert.ToInt32(id_movimiento));//EL METODO LO NECESITA INT
        }

        private void btn_agregarm_Click(object sender, EventArgs e)
        {
            ControlBotones(false, true, true, false);
            ControlCuadros(true);
            txt_nombrem.Focus();
        }
        private void guardarmov()
        {
            _movimientoManejador.guardar(_movimiento);//sacar el usuario para mandarlo llamar en otro lugar
        }


        private void Cargarmov()//crear global el objeto para usarlo en otro lugar 
        {
            _movimiento.Id_movimiento = Convert.ToInt32(txt_lbl.Text);
            _movimiento.Nombre= txt_nombrem.Text;

        }
        private void btn_guardarm_Click(object sender, EventArgs e)
        {
            Cargarmov();//primero se carga el usuario 

            
                ControlBotones(true, false, false, true);
                ControlCuadros(false);
                try
                {

                    guardarmov();//se lo pasamos a guardar usuario 
                    limpiarCuadros();
                    Buscarmov(""); // mandar llamar al data grip con los nuevos datos
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }

        private void txt_lbl_Click(object sender, EventArgs e)
        {
            ControlBotones(true, false, false, true);
            ControlCuadros(false);
            limpiarCuadros();
        }

        private void txt_nombrem_TextChanged(object sender, EventArgs e)
        {
            Buscarmov(txt_buscarmov.Text); //filtrar en la busqueda por letra 

        }

        private void btn_eliminarm_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Estas seguro que deseas elimiar este registro", "Eliminar Resgistro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    Eliminarmov();
                    Buscarmov("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void dtg_movimiento_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Modificarmov();
                Buscarmov("");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
