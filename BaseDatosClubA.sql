create database CLUB_AJEDREZ;

/*Creacion de tablas*/

create table pais(id_pais int  auto_increment primary key,
nombre varchar(25),
cantclub int,
fk_id_pais int,
foreign key (fk_id_pais) references pais(id_pais));

/**/
create table hotel( id_hotel int primary key auto_increment,
nombre varchar(15),
direccion varchar(30),
id sala int,
telefono varchar(10));

create table sala(id_sala int  auto_increment,
capacidad int,
medios varchar(30),
fk_id_hotel int,
foreign  key(fk_id_hotel) references hotel(id_hotel),
primary key(fk_id_hotel,id_sala));

create table reservacion(id_reservacion int primary key auto_increment,
fecha_entrada date,
fecha_salida date,
fk_hotel int,
fk_participante int,
foreign key(fk_hotel) references hotel(id_hotel),
foreign key(fk_participante)references participante(id_participante));


create table jugador (id_jugador int primary key auto_increment,niveljuego int
foreign key (id_jugador) references participante(id_participante);

create table arbitro (id_arbitro int primary key
foreign key(id_arbitro) references arbitro(id_arbitro);




/**/
create table participante(id_participante int primary key auto_increment,
nombre varchar(20),
direccion varchar(30),
telefono varchar(10),
tipo int));

/**/
create table movimiento(id_movimiento int primary key auto_increment,nombre varchar(20));

create table partida(id_partida int primary key auto_increment,id_movimiento int ,id_arbitro
foreign key (id_movimiento) references movimiento(id_movimiento),
foreign key (id_arbitro) references arbitro(id_arbbitro));






















