﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntidadClubA
{
    public class Pais
    {
        private int id_pais;
        private string nombre;
        private int cantclub;
        private int fk_pais;

        public int Id_pais { get => id_pais; set => id_pais = value; }
        public string Nombre { get => nombre; set => nombre = value; }
        public int Cantclub { get => cantclub; set => cantclub = value; }
        public int Fk_pais { get => fk_pais; set => fk_pais = value; }
    }
}
